package.path = package.path .. ';./lib/?.lua;./lib/spec/?.lua;'

local test = require('gambiarra')
local CC = require('cc')

local passed, failed, errored = 0, 0, 0
test(function(ev, fn, msg)
  if ev == 'begin' then
    io.write(string.format('-- Tests started for: %s\n', fn))
  elseif ev == 'pass' then
    passed = passed + 1
    io.write(string.format('[[32m✓[0m] %s\n', msg))
  elseif ev == 'fail' then
    failed = failed + 1
    io.write(string.format('[[31m✗[0m] %s\n', msg))
  elseif ev == 'except' then
    errored = errored + 1
    io.write(string.format('[[33m![0m] %s\n', msg))
  elseif ev == 'end' then
    io.write('\n')
  end
end)

local function report()
  local count = passed + failed + errored
  local stats = string.format('Passed: %d/%d; Failed: %d/%d; Errored: %d/%d', passed, count, failed,
    count, errored, count)
  if passed == count then
    io.write(string.format('--[ [32m%s[0m ]--\n\n', stats))
    os.exit(0)
  else
    io.write(string.format('--[ [31m%s[0m ]--\n\n', stats))
    os.exit(1)
  end
end

local function cc1(ch)
  return { token = tostring(ch) }
end

io.write('--- Started tests for coding challenge module\n\n')

test('new()', function()
  local cc = CC.new(1)

  ok(eq(cc.challenges_url, 'https://cc.the-morpheus.de/challenges/1/'), 'Challenge url was created correctly')
  ok(eq(cc.solutions_url, 'https://cc.the-morpheus.de/solutions/1/'), 'Solutions url was created correctly')
  ok(eq(cc.nojson, false), 'nojson option is false by default')

  cc = nil
  cc = CC.new(1, { nojson = true, get_path = 'sorted' })

  ok(eq('https://cc.the-morpheus.de/challenges/1/sorted/', cc.challenges_url), 'Given get_path is attached to challenges URL')
  ok(eq(cc.nojson, true), 'nojson option was set to true')
end)

test('fetch()', function()
  local cc = CC.new(1, { nojson = true })
  local res = cc:fetch()

  ok(eq(type(res), 'string'), 'Challenge was fetched correctly')
end)

test('solve()', function()
  local cc = CC.new(1, { nojson = true })
  local res, s = cc:solve(cc1)

  ok(eq(type(res), 'table'), 'Solutions is a table')
  ok(eq(res.token ~= nil, true), 'Solutions has key "token"')

  res, s = cc:solve(cc1, true)

  ok(eq(res:find('Success') ~= nil, true), 'After solution was solved it was submitted successfully')
  ok(eq(s, 200), 'Status code is 200')
end)

test('submit()', function()
  local cc = CC.new(1, { nojson = true })
  local res, s = cc:submit(cc:solve(cc1))

  ok(eq(res:find('Success') ~= nil, true), 'Solution was submitted successfully')
  ok(eq(s, 200), 'Status code is 200')
end)

test('runtime_avg()', function()
  local cc = CC.new(1, { nojson = true })
  cc:solve(cc1, true)

  ok(eq(cc:runtime_avg():match('%d+%.%d+') ~= nil, true), 'Runtime was returned')
end)

report()