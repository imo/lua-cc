# Coding Challenge Modul

[![Build Status](https://drone.kokolor.es/api/badges/imo/lua-cc/status.svg)](https://drone.kokolor.es/imo/lua-cc)

Eine kleines Modul, zum Challenge fetchen und submitten. Außerdem kann man sich die durchschnittliche Dauer für alle Lösungsdurchläufe ausgeben lassen.

Es wurde von mir geschrieben um wiederkehrende Aufgaben wie fetchen, submitten etc. beim Lösen der Coding Challenges vom Youtube Channel [The Morpheus Tutorials](https://www.youtube.com/user/TheMorpheus407) zu vereinfachen. Meine Lösungen die dieses Modul nutzen liegen hier: [coding_challenge_loesung](https://git.kokolor.es/imo/coding_challenge_loesung)

## Warning

I am not a programmer, so here's a warning: **This code was written in an exploratory way.** If you encounter problems, see something wrong or something was implemented in a weird way, I would be happy if you tell me about it or create a pull request. Thank you. :)

## Nutzungsbeispiel

```lua
> CC = require('cc')
> cc = CC.new(1, { nojson = true })
> cc:solve(function(ch) return { token = tostring(ch) } end, true)
Success   200
> cc:runtime_avg()
0.000003s
```

## Inhalt

* [Abhängigkeiten](#abhängigkeiten)
* [Funktionen](#funktionen)
   * [new()](#newcn-opts)
   * [fetch()](#fetch)
   * [solve()](#solvefn-sbmt)
   * [submit()](#submitdata-header)
   * [runtime_avg()](#runtime_avg)
* [Beispiele](#beispiele)
* [Tests](#tests)

## Abhängigkeiten

* lua >= 5.1
* [lua-curl](https://github.com/Lua-cURL/Lua-cURLv3)
* [json.lua](https://github.com/rxi/json.lua) or [dkjson](http://dkolf.de/src/dkjson-lua.fsl/home)
* [chronos](https://github.com/ldrumm/chronos)

## Funktionen

### `new(cn[, opts])`

Erstellt neues Coding Challenge Instanz.

**Parameter:**

* _cn:_ (**number**) Nummer der Challenge, also z.B. 3 für die dritte Challenge
* _opts:_ (**table**) options (**opt**)
* _opts.nojson:_ (**bool**) auf `true` setzen wenn die Challenge nicht als JSON bereitgestellt wird
* _opts.get_path:_ (**string**) der Pfad wird der get URL hinzugefügt. Bei einer Challenge gab es mal die Möglichkeit die Werte sortiert übermittelt zu bekommen indem mal `/sorted` als Pfad angab.
* _opts.post_path:_ (**string**) selbe wie `get_path` nur `POST` request, also `submit()`

**Returns:**

(**table**) Objekt table

**Usage:**

```lua
local cc = CC.new(15)
```

### `fetch()`

Abrufen einer Challenge.

**Parameter:**

`nil`

**Returns:**

(**table|string**) wenn `opts.nojson` auf `true` gesetzt ist, wird ein String returned, sonst eine Table.

**Usage:**

```lua
local challenge = cc:fetch()
```

### `solve(fn, sbmt)`

Lösen der Challenge. Dies ist eigentlich nur ein Wrapper für die Lösungsfunktion, welcher die Ausführungszeit misst. Wird dies nicht benötigt kann man auch die Lösungsfunktion ohne `solve()` nutzen.
`solve()` ruft `fetch()` auf um eine neue Challenge zu bekommen. Sollte man also eine `for`-Loop nutzen, brauch man nur `solve()` nutzen.

**Parameter:**

* _fn:_ (**function**) die Funktion, welche die Challenge löst. `fn` nimmt ein Parameter (_table_) entgegen.
* _sbmt:_ (**bool**) Wenn `true` wird direkt `submit()` mit dem zurückgegebenen Wert von `fn` aufgerufen.

**Returns:**

(**void** | **string**, **number**) Das Ergebnis | Wenn `sbmt = true`

**Usage:**

```lua
local result = cc:solve(function(c) return c.word end)
```

### `submit(data[, header])`

Übermitteln der Lösung.

**Parameter:**

* _data:_ (**table**) Übergabe der Daten in Form einer table, welche dann in JSON umgewandelt wird.
* _headers:_ (**table**) Werden zusätzliche Header benötigt, können diese als Tabelle übergeben werden

**Returns:**

(**string**, **number**) Return value 1 ist der Status Text ob _Success_ oder _Error_ und value 2 ist der HTTP Statuscode

**Usage:**

```lua
local resp = cc:post({ token = 'blubb' })
```

```lua
local c2 = c2:post({
  token = 'blubb'
},
{
  headers = {
    ['X-Clacks-Overhead'] = 'GNU Terry Pratchett'
  }
})
```

### `runtime_avg()`

Durchschnittliche Zeit aller Lösungs Durchläufe. Für Benchmark.

**Parameter:**

`nil`

**Returns:**

(**string**) the time in seconds

**Usage:**

```lua
cc:runtime_avg() -- Output: 0.000004
```

## Beispiele

* C15: [palindrome.lua](https://git.kokolor.es/imo/coding_challenge_loesung/src/branch/master/c15/palindrome.lua)

## Tests

Um die Tests laufen zu lassen wird [gambiarra](https://codeberg.org/imo/gambiarra) benötigt. Einfach `gambiarra.lua` herunterladen und in den Ordner `spec/` packen und die Tests mit `lua lib/spec/cc_spec.lua` ausführen.
